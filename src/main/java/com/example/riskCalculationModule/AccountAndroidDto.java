package com.example.riskCalculationModule;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccountAndroidDto
{
    private String login;
    private String name;
    private String surname;
    private String password;
    private String role;
    private String email;
    private String yearOfBirth;
}
