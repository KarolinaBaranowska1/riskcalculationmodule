package com.example.riskCalculationModule;


import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountDao extends JpaRepository<AccountEntity,Long>
{
    AccountEntity findFirstByEmail(String email);
}
