package com.example.riskCalculationModule;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountEntity {
    private String login;
    private String name;
    private String surname;
    private String password;
    private String role;
    @Column
    private String email;
    private String yearOfBirth;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
}
