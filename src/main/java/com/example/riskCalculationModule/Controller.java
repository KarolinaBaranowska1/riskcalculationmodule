package com.example.riskCalculationModule;

import com.example.riskCalculationModule.linked.data.LinkedEntity;
import com.example.riskCalculationModule.linked.service.LinkedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.ExecutionException;

@RestController
public class Controller {

    @Autowired
    private FacebookDataService facebookDataService;
    @Autowired
    private RiskCalculationService riskCalculationService;
    @Autowired
    private LinkedService linkedService;
    @Autowired
    private TwitterService twitterService;

    @PostMapping("dane")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void fbData(@RequestBody FacebookAndroidData facebookAndroidData) {
        facebookDataService.facebookDataToDatabase(facebookAndroidData);
    }

    @GetMapping("dane3/{login}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public AccountEntity getUserAccount(@PathVariable String login) {
        AccountEntity userFromAccountDb = riskCalculationService.getUserFromAccountDb(login);
        return userFromAccountDb;
    }

    @PostMapping("/dateOfDataSending")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void date(@RequestBody Date date) {

    }

    @PostMapping("transactions/{transactionsCount}/{email}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void getTransactionsCount(@PathVariable Integer transactionsCount, @PathVariable String email) {
        riskCalculationService.tranToDatabase(TransactionsCount.builder().numberOfTransactions(transactionsCount).email(email).build());
    }

//    @GetMapping("dane4/{fbUserId}")
//    @ResponseStatus(HttpStatus.ACCEPTED)
//    public FacebookUserEntity getFacebookUserAccount(@PathVariable Long fbUserId)
//    {
//        FacebookUserEntity facebookUserEntity = riskCalculationService.getFacebookUserFromFacebookDb(fbUserId);
//        return facebookUserEntity;
//    }

    @GetMapping("ryzyko/{appEmail}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Risk risk(@PathVariable String appEmail) {
        FacebookUserEntity facebookUserEntity = riskCalculationService.getFacebookByAppEmailAddress(appEmail);
        AccountEntity user = riskCalculationService.getUserFromAccountDb(appEmail);
        LinkedEntity linkedUser = riskCalculationService.getLinkedUserByAppEmailAddress(appEmail);
        TwitterUserEntity twitterUserEntity = riskCalculationService.getTwitterUserByAppEmailAddress(appEmail);
        return Risk.builder()
                .riskCalculated((100 - riskCalculationService.loanRisk(appEmail, user, facebookUserEntity, linkedUser, twitterUserEntity) * 100 / 26))
                .facebookDate(facebookUserEntity != null ? facebookUserEntity.getDate() : null)
                .linkedDate(linkedUser != null ? linkedUser.getDate() : null)
                .twitterDate(twitterUserEntity != null ? twitterUserEntity.getDate() : null)
                .build();
    }

    @GetMapping("daneKonto/{token}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void getAccessTokenFromLoginService(@PathVariable String token) {
        facebookDataService.getAccessTokenFromLoginService(token);
    }


    @GetMapping("linked/{token}/{appEmailAddress}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void saveInfoFromLinked(@PathVariable String token, @PathVariable String appEmailAddress) {
        linkedService.saveLinkedInfo(token, appEmailAddress);
    }

    @PostMapping("/twitterData")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void twitterData(@RequestBody TwitterAndroidData twitterAndroidData) throws InterruptedException, ExecutionException, IOException {
        twitterService.twitterDataToDatabase(twitterAndroidData);
    }

}
