package com.example.riskCalculationModule;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;


@Data //getery settery
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FacebookAndroidData //from android for my service in order to get facbeook account info here
{
    @Length(min = 1)
    private String firstName;
    @Length(min = 1)
    private String lastName;
    @Length(min = 1)
    private String middleName;
    private Long fbUserId;
    private String appEmailaddress;
    private String accessToken;

}
