package com.example.riskCalculationModule;

import com.example.riskCalculationModule.facebookModel.Datum;
import com.example.riskCalculationModule.facebookModel.DetailedDataFromFacebook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FacebookDataService
{
    @Autowired
    AccountDao accountDao;
    @Autowired
    FacebookFriendDao facebookFriendDao;
    @Autowired
    FacebookUserDao facebookUserDao;

    public void facebookDataToDatabase(FacebookAndroidData facebookAndroidData)
    {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization", "Bearer " + facebookAndroidData.getAccessToken());
        String url = "https://graph.facebook.com/v5.0/" + facebookAndroidData.getFbUserId() + "/friends";
        HttpEntity<String> entity = new HttpEntity<String>("parameters", httpHeaders);

        ResponseEntity<DetailedDataFromFacebook> fbRespone = restTemplate.exchange(url, HttpMethod.GET, entity, DetailedDataFromFacebook.class);
        List<Datum> mapOfFriends = fbRespone.getBody().getData();

        facebookUserDao.save
                (
                        FacebookUserEntity
                                .builder()
                                .firstName(facebookAndroidData.getFirstName())
                                .lastName(facebookAndroidData.getLastName())
                                .middleName(facebookAndroidData.getMiddleName())
                                .accessToken(facebookAndroidData.getAccessToken())
                                .fbUserId(facebookAndroidData.getFbUserId())
                                .appEmailaddress(facebookAndroidData.getAppEmailaddress())
                                .lisOfUserFriends(mapOfFriends.stream().map(
                                        datum -> FacebookFriendEntity
                                                .builder()
                                                .fbUserId(Long.valueOf(datum.getId()))
                                                .name(datum.getName())
                                                .build())
                                        .collect(Collectors.toList()))
                                .build()
                );
    }

    public void getAccessTokenFromLoginService(String token)
    {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(token);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON)); //co odbieram
        headers.setContentType(MediaType.APPLICATION_JSON); //co ja wysylam
        headers.add("user-agent", "Mozilla/5.0 Firefox/26.0");

        HttpEntity<String> entity = new HttpEntity<>("parameters", headers);

        ResponseEntity<AccountAndroidDto> response = restTemplate.exchange("https://jwtservice.herokuapp" +
                        ".com/get", HttpMethod.GET,
                entity, AccountAndroidDto.class);

        AccountAndroidDto response2 = response.getBody(); //zapis do bazy danych

        accountDao.save
                (AccountEntity.builder()
                .email(response2.getEmail())
                .name(response2.getName())
                .login(response2.getLogin())
                .yearOfBirth(response2.getYearOfBirth())
                .surname(response2.getSurname())
                .role(response2.getRole())
                .build()
                );
    }
}
