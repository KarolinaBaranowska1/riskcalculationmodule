package com.example.riskCalculationModule;

import org.springframework.data.jpa.repository.JpaRepository;

public interface FacebookFriendDao extends JpaRepository<FacebookFriendEntity, Long>
{
    //FacebookFriendEntity findAccountEntitiesBy(String login);
}
