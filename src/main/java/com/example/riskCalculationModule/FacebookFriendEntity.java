package com.example.riskCalculationModule;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Entity;
import javax.persistence.Id;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Data
@Builder
public class FacebookFriendEntity
{
    @Id
    private Long fbUserId;
    @Length(min = 1)
    private String name;
}
