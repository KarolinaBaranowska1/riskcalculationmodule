package com.example.riskCalculationModule;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FacebookRiskCalculated
{
    private Integer risk;
}
