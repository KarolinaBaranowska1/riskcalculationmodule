package com.example.riskCalculationModule;

import org.springframework.data.jpa.repository.JpaRepository;

public interface FacebookUserDao extends JpaRepository<FacebookUserEntity, Long> {
    FacebookUserEntity findAccountEntitiesByFbUserId(Long fbUserId);

    FacebookUserEntity findAccountEntitiesByFirstName(String firstName);

    FacebookUserEntity findByAppEmailaddress(String appEmailAddress);

}
