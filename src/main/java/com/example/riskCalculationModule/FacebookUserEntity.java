package com.example.riskCalculationModule;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Data
@Builder
public class FacebookUserEntity
{

    private String firstName;

    private String lastName;

    private String middleName;
    @Id
    private Long fbUserId;

    @Column(length = 300)
    private String accessToken;

    @CreationTimestamp
    private Date date;

    private String appEmailaddress;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true) //jeden user bedzie polaczony z wieloma friendami
    private List<FacebookFriendEntity> lisOfUserFriends;
}
