package com.example.riskCalculationModule;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Builder
public class LinkedInJobEntity
{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long jobId;
    private String companyName;
    private Integer startYear;
    private Integer startMonth;
}
