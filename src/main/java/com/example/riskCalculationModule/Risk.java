package com.example.riskCalculationModule;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Risk {
    private Integer riskCalculated;
    private Date linkedDate;
    private Date facebookDate;
    private Date twitterDate;
}