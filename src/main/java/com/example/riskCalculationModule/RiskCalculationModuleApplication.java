package com.example.riskCalculationModule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RiskCalculationModuleApplication
{

	public static void main(String[] args)
	{
		SpringApplication.run(RiskCalculationModuleApplication.class, args);
	}

}
