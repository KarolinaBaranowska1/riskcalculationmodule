package com.example.riskCalculationModule;

import com.example.riskCalculationModule.linked.LinkedDao;
import com.example.riskCalculationModule.linked.data.LinkedEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class RiskCalculationService {
    @Autowired
    AccountDao accountDao;
    @Autowired
    FacebookFriendDao facebookFriendDao;
    @Autowired
    FacebookUserDao facebookUserDao;
    @Autowired
    LinkedDao linkedDao;
    @Autowired
    TwitterUserDao twitterUserDao;
    @Autowired
    TransactionsDao transactionsDao;


    public AccountEntity getUserFromAccountDb(String email) {
        return accountDao.findFirstByEmail(email);
    }


    public FacebookUserEntity getFacebookByAppEmailAddress(String appEmailAddress) {
        return facebookUserDao.findByAppEmailaddress(appEmailAddress);
    }

    public LinkedEntity getLinkedUserByAppEmailAddress(String appEmailAddress) {
        return linkedDao.findFirstByAppEmailAddress(appEmailAddress);
    }

    public TwitterUserEntity getTwitterUserByAppEmailAddress(String appEmailAddress) {
        return twitterUserDao.findFirstByAppEmailaddress(appEmailAddress);
    }


    public LinkedEntity getLinkedInUserFromDb(String email) {
        return linkedDao.findByEmail(email);
    }

    public TwitterUserEntity getTwitterUserFromDb(String name) {
        return twitterUserDao.findByName(name);
    }

    public Integer loanRisk(String email, AccountEntity accountEntity, FacebookUserEntity facebookUserEntity, LinkedEntity linkedEntity, TwitterUserEntity twitterUserEntity) {
        Integer transactionsCount = 0;
        try {
            transactionsCount = transactionsDao.findFirstByEmail(email).transactionsCount;
        } catch (Exception e) {

        }
        if (transactionsCount == null) {
            transactionsCount = 0;
        }
        if (transactionsCount > 10) {
            transactionsCount = 10;
        }
        return compareAccountAndFacebookNames(accountEntity, facebookUserEntity)
                + compareLinkedAndFacebookNames(facebookUserEntity, linkedEntity)
                + compareAccountAndLinkedNames(accountEntity, linkedEntity)
                + hasTwitterFriends(twitterUserEntity)
                + hasAJob(linkedEntity)
                + wasTwitterCreatedRecently(twitterUserEntity)
                + hasFacebookFriends(facebookUserEntity)
                + compareTwitterAndOthersNames(accountEntity, facebookUserEntity, linkedEntity, twitterUserEntity)
                + transactionsCount;
    }

    public boolean isTwitterNull(TwitterUserEntity twitterUserEntity) {
        return twitterUserEntity.equals(null);
    }

    public boolean isLinkedNull(LinkedEntity linkedUserEntity) {
        return linkedUserEntity.equals(null);
    }

    public boolean isFacebookNull(FacebookUserEntity facebookUserEntity) {
        return facebookUserEntity.equals(null);
    }

    public boolean isAnySocialServiceData(TwitterUserEntity twitterUserEntity, LinkedEntity linkedUserEntity, FacebookUserEntity facebookUserEntity) {
        return !(isTwitterNull(twitterUserEntity) || isLinkedNull(linkedUserEntity) || isFacebookNull(facebookUserEntity));
    }

    public Integer compareTwitterAndOthersNames(AccountEntity accountEntity, FacebookUserEntity facebookUserEntity, LinkedEntity linkedEntity, TwitterUserEntity twitterUserEntity) {
        try {
            String accountName = accountEntity.getName() + accountEntity.getSurname();
            String facebookName = facebookUserEntity.getFirstName() + facebookUserEntity.getLastName();
            String linkedName = linkedEntity.getName() + linkedEntity.getSurname();
            String twitterName = twitterUserEntity.getName();

            if (accountName.equals(twitterName) || facebookName.equals(twitterName) || linkedName.equals(twitterName)) {
                return 2;
            } else
                return 0;
        } catch (Exception e) {
            return 0;
        }

    }

    public Integer compareAccountAndFacebookNames(AccountEntity accountEntity, FacebookUserEntity facebookUserEntity) {

        try {
            if (accountEntity.getName().equals(facebookUserEntity.getFirstName())) {
                if (accountEntity.getSurname().equals(facebookUserEntity.getLastName())) {
                    return 2;
                } else
                    return 1;
            } else
                return 0;
        } catch (Exception e) {
            return 0;
        }
    }

    public Integer compareLinkedAndFacebookNames(FacebookUserEntity facebookUserEntity, LinkedEntity linkedEntity) {
        try {
            if (linkedEntity.getName().equals(facebookUserEntity.getFirstName())) {
                if (linkedEntity.getSurname().equals(facebookUserEntity.getLastName())) {
                    return 2;
                } else
                    return 1;
            } else
                return 0;

        } catch (Exception e) {
            return 0;
        }
    }

    public Integer compareAccountAndLinkedNames(AccountEntity accountEntity, LinkedEntity linkedEntity) {
        try {
            if (accountEntity.getName().equals(linkedEntity.getName())) {
                if (accountEntity.getSurname().equals(linkedEntity.getSurname())) {
                    return 2;
                } else
                    return 1;
            } else
                return 0;

        } catch (Exception e) {
            return 0;
        }

    }


    public Integer hasTwitterFriends(TwitterUserEntity twitterUserEntity) {
        try {
            if ((twitterUserEntity.getFollowersCount() > 0) && (twitterUserEntity.getFriendsCount() > 0) && (twitterUserEntity.getListedCount() > 0)) {
                return 2;
            } else if ((twitterUserEntity.getFollowersCount() > 0) || (twitterUserEntity.getFriendsCount() > 0) || (twitterUserEntity.getListedCount() > 0)) {
                return 1;
            } else
                return 0;
        } catch (Exception e) {
            return 0;
        }
    }

    public Integer hasAJob(LinkedEntity linkedEntity) {
        try {
            if (linkedEntity.getListOfUsersJobs().size() > 5) {
                return 2;
            } else if (linkedEntity.getListOfUsersJobs().size() == 1) {
                return 1;
            } else
                return 0;
        } catch (Exception e) {
            return 0;
        }
    }

    public LocalDate twitterDateParser(TwitterUserEntity twitterUserEntity) {
        Integer mon = 10;

        if ("nov".equalsIgnoreCase(twitterUserEntity.getCreatedAt().substring(4, 7))) {
            mon = 1;
        }

        return LocalDate.of
                (
                        Integer.parseInt(twitterUserEntity.getCreatedAt().substring(26)),
                        mon,
                        Integer.parseInt(twitterUserEntity.getCreatedAt().substring(8, 10))
                );
    }

    public Integer wasTwitterCreatedRecently(TwitterUserEntity twitterUserEntity) {
        try {
            LocalDate createdAt = twitterDateParser(twitterUserEntity);
            createdAt = createdAt.plusMonths(3L);
            LocalDate now = LocalDate.now();
            if (createdAt.isBefore(now)) {
                return 2;
            } else
                return 0;
        } catch (Exception e) {
            return 0;
        }
    }

    public Integer hasFacebookFriends(FacebookUserEntity facebookUserEntity) {
        try {
            if (facebookUserEntity.getLisOfUserFriends().size() > 3) {
                return 2;
            } else
                return 0;
        } catch (Exception e) {
            return 0;
        }
    }

    public void tranToDatabase(TransactionsCount transactionsCount) {
        transactionsDao.save(TransactionsCountEntity.builder()
                .date(transactionsCount.getDate())
                .email(transactionsCount.getEmail())
                .transactionsCount(transactionsCount.getNumberOfTransactions())
                .build());
    }
}
