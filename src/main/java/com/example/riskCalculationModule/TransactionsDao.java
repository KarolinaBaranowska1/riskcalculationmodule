package com.example.riskCalculationModule;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionsDao extends JpaRepository<TransactionsCountEntity, Long> {
    TransactionsCountEntity findFirstByDate(String date);

    TransactionsCountEntity findFirstByEmail(String email);
}
