package com.example.riskCalculationModule;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class TwitterAndroidData
{
        private String consumerKey;
        private String consumerSecretKey;
        private String accessToken;
        private String accessTokenSecret;
    private String appEmailaddress;
}
