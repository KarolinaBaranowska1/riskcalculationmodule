package com.example.riskCalculationModule;

import com.example.riskCalculationModule.twitter.TwitterData;
import com.google.gson.Gson;
import org.scribe.builder.ServiceBuilder;
import org.scribe.model.*;
import org.scribe.oauth.OAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TwitterService
{

    @Autowired
    private TwitterUserDao twitterUserDao;

    public void twitterDataToDatabase(TwitterAndroidData twitterAndroidData) {

        OAuthService oauthService = new ServiceBuilder()
                .provider(TradeKingAPI.class)
                .apiKey("CXeo1apdMeRh3dpc2VuC1JkMl")
                .apiSecret("ZoM1I9iB5dm38yyr0JkjMQYIQDWlVTxoMB9i4EhZ51QlOYBXt9")
                .build();

        Long seconds = (System.currentTimeMillis() / 1000);
        System.out.println(">>>" + seconds);
        String stSeconds = seconds.toString();
        OAuthRequest request = new OAuthRequest(Verb.GET, "https://api.twitter.com/1.1/account/verify_credentials.json");

        request.addOAuthParameter(OAuthConstants.CONSUMER_KEY, "zGbb5boneN26GFDdhrZqQJ57V");
        request.addOAuthParameter(OAuthConstants.CONSUMER_SECRET, "TKEnXUZDpF8Hezm1kZ8XlkqRMbURkAm4WltvggA1SNBpvQ0Pov");
        request.addOAuthParameter(OAuthConstants.NONCE, "myn0nc3");
        request.addOAuthParameter(OAuthConstants.SIGN_METHOD, "HMAC-SHA1");
        request.addOAuthParameter(OAuthConstants.TIMESTAMP, seconds.toString());
        request.addOAuthParameter(OAuthConstants.VERSION, "1.0");

        Token token = new Token(twitterAndroidData.getAccessToken(), twitterAndroidData.getAccessTokenSecret());

        oauthService.signRequest(token, request);
        Response response = request.send();
        System.err.println(">>" + response.isSuccessful());
        System.err.println(">>" + response.getMessage());
        System.err.println(">>" + response.getBody());


        Gson gson = new Gson(); // Or use new GsonBuilder().create();
        TwitterData twitterData = gson.fromJson(response.getBody(), TwitterData.class);

        twitterUserDao.save(
                TwitterUserEntity
                        .builder()
                        .name(twitterData.getName())
                        .screenName(twitterData.getScreenName())
                        .twitterUseId(twitterData.getId())
                        .friendsCount(twitterData.getFriendsCount())
                        .location(twitterData.getLocation())
                        .followersCount(twitterData.getFollowersCount())
                        .listedCount(twitterData.getListedCount())
                        .createdAt(twitterData.getCreatedAt())
                        .appEmailaddress(twitterAndroidData.getAppEmailaddress())
                        .build()
        );
    }


}
