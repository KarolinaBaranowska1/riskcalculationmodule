package com.example.riskCalculationModule;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TwitterUserDao extends JpaRepository<TwitterUserEntity, Long> {
    //FacebookUserEntity findAccountEntitiesByFbUserId(Long fbUserId);
    TwitterUserEntity findByName(String name);

    TwitterUserEntity findFirstByAppEmailaddress(String appEmailAddress);

}
