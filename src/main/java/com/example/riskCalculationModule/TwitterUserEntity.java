package com.example.riskCalculationModule;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Data
@Builder
public class TwitterUserEntity
{
    @Id
    @GeneratedValue
    private Long dbId;
    private Long twitterUseId;
    private String name;
    private String screenName;
    private String location;
    private Long followersCount;
    private Long friendsCount;
    private Long listedCount;
    private String createdAt;
    private String appEmailaddress;
    @CreationTimestamp
    private Date date;
}
