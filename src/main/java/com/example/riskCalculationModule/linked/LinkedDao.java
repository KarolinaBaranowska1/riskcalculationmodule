package com.example.riskCalculationModule.linked;

import com.example.riskCalculationModule.linked.data.LinkedEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LinkedDao extends JpaRepository<LinkedEntity, Long> {
    LinkedEntity findByEmail(String email);
    LinkedEntity findFirstByAppEmailAddress(String appEmailAddress);
}
