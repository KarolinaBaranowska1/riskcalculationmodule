package com.example.riskCalculationModule.linked.data;

import com.example.riskCalculationModule.LinkedInJobEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class LinkedEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private String name;
    private String surname;
    private String email;
    private String appEmailAddress;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<LinkedInJobEntity> listOfUsersJobs;
    @CreationTimestamp
    private Date date;
}
