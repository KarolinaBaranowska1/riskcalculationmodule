
package com.example.riskCalculationModule.linked.data.linkedModel;

import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "localized",
    "preferredLocale"
})
public class FirstName {

    @JsonProperty("localized")
    @Valid
    private Localized localized;
    @JsonProperty("preferredLocale")
    @Valid
    private PreferredLocale preferredLocale;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public FirstName() {
    }

    /**
     * 
     * @param localized
     * @param preferredLocale
     */
    public FirstName(Localized localized, PreferredLocale preferredLocale) {
        super();
        this.localized = localized;
        this.preferredLocale = preferredLocale;
    }

    @JsonProperty("localized")
    public Localized getLocalized() {
        return localized;
    }

    @JsonProperty("localized")
    public void setLocalized(Localized localized) {
        this.localized = localized;
    }

    public FirstName withLocalized(Localized localized) {
        this.localized = localized;
        return this;
    }

    @JsonProperty("preferredLocale")
    public PreferredLocale getPreferredLocale() {
        return preferredLocale;
    }

    @JsonProperty("preferredLocale")
    public void setPreferredLocale(PreferredLocale preferredLocale) {
        this.preferredLocale = preferredLocale;
    }

    public FirstName withPreferredLocale(PreferredLocale preferredLocale) {
        this.preferredLocale = preferredLocale;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public FirstName withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("localized", localized).append("preferredLocale", preferredLocale).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(localized).append(additionalProperties).append(preferredLocale).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof FirstName) == false) {
            return false;
        }
        FirstName rhs = ((FirstName) other);
        return new EqualsBuilder().append(localized, rhs.localized).append(additionalProperties, rhs.additionalProperties).append(preferredLocale, rhs.preferredLocale).isEquals();
    }

}
