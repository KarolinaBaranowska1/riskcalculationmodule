
package com.example.riskCalculationModule.linked.data.linkedModel;

import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "firstName",
    "lastName",
    "id"
})
public class LinkedNameAndSurname {

    @JsonProperty("firstName")
    @Valid
    private FirstName firstName;
    @JsonProperty("lastName")
    @Valid
    private LastName lastName;
    @JsonProperty("id")
    private String id;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public LinkedNameAndSurname() {
    }

    /**
     * 
     * @param firstName
     * @param lastName
     * @param id
     */
    public LinkedNameAndSurname(FirstName firstName, LastName lastName, String id) {
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        this.id = id;
    }

    @JsonProperty("firstName")
    public FirstName getFirstName() {
        return firstName;
    }

    @JsonProperty("firstName")
    public void setFirstName(FirstName firstName) {
        this.firstName = firstName;
    }

    public LinkedNameAndSurname withFirstName(FirstName firstName) {
        this.firstName = firstName;
        return this;
    }

    @JsonProperty("lastName")
    public LastName getLastName() {
        return lastName;
    }

    @JsonProperty("lastName")
    public void setLastName(LastName lastName) {
        this.lastName = lastName;
    }

    public LinkedNameAndSurname withLastName(LastName lastName) {
        this.lastName = lastName;
        return this;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    public LinkedNameAndSurname withId(String id) {
        this.id = id;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public LinkedNameAndSurname withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("firstName", firstName).append("lastName", lastName).append("id", id).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(firstName).append(lastName).append(id).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof LinkedNameAndSurname) == false) {
            return false;
        }
        LinkedNameAndSurname rhs = ((LinkedNameAndSurname) other);
        return new EqualsBuilder().append(firstName, rhs.firstName).append(lastName, rhs.lastName).append(id, rhs.id).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
