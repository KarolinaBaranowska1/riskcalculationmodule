
package com.example.riskCalculationModule.linked.data.linkedModel;

import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "pl_PL"
})
public class Localized_ {

    @JsonProperty("pl_PL")
    private String plPL;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Localized_() {
    }

    /**
     * 
     * @param plPL
     */
    public Localized_(String plPL) {
        super();
        this.plPL = plPL;
    }

    @JsonProperty("pl_PL")
    public String getPlPL() {
        return plPL;
    }

    @JsonProperty("pl_PL")
    public void setPlPL(String plPL) {
        this.plPL = plPL;
    }

    public Localized_ withPlPL(String plPL) {
        this.plPL = plPL;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Localized_ withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("plPL", plPL).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(plPL).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Localized_) == false) {
            return false;
        }
        Localized_ rhs = ((Localized_) other);
        return new EqualsBuilder().append(plPL, rhs.plPL).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
