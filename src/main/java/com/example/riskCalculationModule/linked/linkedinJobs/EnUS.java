
package com.example.riskCalculationModule.linked.linkedinJobs;

import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "rawText"
})
public class EnUS {

    @JsonProperty("rawText")
    private String rawText;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public EnUS() {
    }

    /**
     * 
     * @param rawText
     */
    public EnUS(String rawText) {
        super();
        this.rawText = rawText;
    }

    @JsonProperty("rawText")
    public String getRawText() {
        return rawText;
    }

    @JsonProperty("rawText")
    public void setRawText(String rawText) {
        this.rawText = rawText;
    }

    public EnUS withRawText(String rawText) {
        this.rawText = rawText;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public EnUS withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("rawText", rawText).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(rawText).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof EnUS) == false) {
            return false;
        }
        EnUS rhs = ((EnUS) other);
        return new EqualsBuilder().append(rawText, rhs.rawText).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
