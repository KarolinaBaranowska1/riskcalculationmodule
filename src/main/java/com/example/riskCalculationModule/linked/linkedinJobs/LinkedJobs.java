
package com.example.riskCalculationModule.linked.linkedinJobs;

import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "startMonthYear",
    "companyName",
    "description",
    "location",
    "company",
    "title"
})
public class LinkedJobs {

    @JsonProperty("startMonthYear")
    @Valid
    private StartMonthYear startMonthYear;
    @JsonProperty("companyName")
    @Valid
    private CompanyName companyName;
    @JsonProperty("description")
    @Valid
    private Description description;
    @JsonProperty("location")
    @Valid
    private Location location;
    @JsonProperty("company")
    private String company;
    @JsonProperty("title")
    @Valid
    private Title title;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public LinkedJobs() {
    }

    /**
     * 
     * @param startMonthYear
     * @param companyName
     * @param description
     * @param location
     * @param company
     * @param title
     */
    public LinkedJobs(StartMonthYear startMonthYear, CompanyName companyName, Description description, Location location, String company, Title title) {
        super();
        this.startMonthYear = startMonthYear;
        this.companyName = companyName;
        this.description = description;
        this.location = location;
        this.company = company;
        this.title = title;
    }

    @JsonProperty("startMonthYear")
    public StartMonthYear getStartMonthYear() {
        return startMonthYear;
    }

    @JsonProperty("startMonthYear")
    public void setStartMonthYear(StartMonthYear startMonthYear) {
        this.startMonthYear = startMonthYear;
    }

    public LinkedJobs withStartMonthYear(StartMonthYear startMonthYear) {
        this.startMonthYear = startMonthYear;
        return this;
    }

    @JsonProperty("companyName")
    public CompanyName getCompanyName() {
        return companyName;
    }

    @JsonProperty("companyName")
    public void setCompanyName(CompanyName companyName) {
        this.companyName = companyName;
    }

    public LinkedJobs withCompanyName(CompanyName companyName) {
        this.companyName = companyName;
        return this;
    }

    @JsonProperty("description")
    public Description getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(Description description) {
        this.description = description;
    }

    public LinkedJobs withDescription(Description description) {
        this.description = description;
        return this;
    }

    @JsonProperty("location")
    public Location getLocation() {
        return location;
    }

    @JsonProperty("location")
    public void setLocation(Location location) {
        this.location = location;
    }

    public LinkedJobs withLocation(Location location) {
        this.location = location;
        return this;
    }

    @JsonProperty("company")
    public String getCompany() {
        return company;
    }

    @JsonProperty("company")
    public void setCompany(String company) {
        this.company = company;
    }

    public LinkedJobs withCompany(String company) {
        this.company = company;
        return this;
    }

    @JsonProperty("title")
    public Title getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(Title title) {
        this.title = title;
    }

    public LinkedJobs withTitle(Title title) {
        this.title = title;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public LinkedJobs withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("startMonthYear", startMonthYear).append("companyName", companyName).append("description", description).append("location", location).append("company", company).append("title", title).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(startMonthYear).append(companyName).append(description).append(location).append(company).append(additionalProperties).append(title).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof LinkedJobs) == false) {
            return false;
        }
        LinkedJobs rhs = ((LinkedJobs) other);
        return new EqualsBuilder().append(startMonthYear, rhs.startMonthYear).append(companyName, rhs.companyName).append(description, rhs.description).append(location, rhs.location).append(company, rhs.company).append(additionalProperties, rhs.additionalProperties).append(title, rhs.title).isEquals();
    }

}
