
package com.example.riskCalculationModule.linked.linkedinJobs;

import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "en_US"
})
public class Localized_ {

    @JsonProperty("en_US")
    @Valid
    private EnUS enUS;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Localized_() {
    }

    /**
     * 
     * @param enUS
     */
    public Localized_(EnUS enUS) {
        super();
        this.enUS = enUS;
    }

    @JsonProperty("en_US")
    public EnUS getEnUS() {
        return enUS;
    }

    @JsonProperty("en_US")
    public void setEnUS(EnUS enUS) {
        this.enUS = enUS;
    }

    public Localized_ withEnUS(EnUS enUS) {
        this.enUS = enUS;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Localized_ withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("enUS", enUS).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(enUS).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Localized_) == false) {
            return false;
        }
        Localized_ rhs = ((Localized_) other);
        return new EqualsBuilder().append(enUS, rhs.enUS).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
