
package com.example.riskCalculationModule.linked.linkedinJobs;

import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "localized",
    "preferredLocale"
})
public class Title {

    @JsonProperty("localized")
    @Valid
    private Localized__ localized;
    @JsonProperty("preferredLocale")
    @Valid
    private PreferredLocale__ preferredLocale;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Title() {
    }

    /**
     * 
     * @param localized
     * @param preferredLocale
     */
    public Title(Localized__ localized, PreferredLocale__ preferredLocale) {
        super();
        this.localized = localized;
        this.preferredLocale = preferredLocale;
    }

    @JsonProperty("localized")
    public Localized__ getLocalized() {
        return localized;
    }

    @JsonProperty("localized")
    public void setLocalized(Localized__ localized) {
        this.localized = localized;
    }

    public Title withLocalized(Localized__ localized) {
        this.localized = localized;
        return this;
    }

    @JsonProperty("preferredLocale")
    public PreferredLocale__ getPreferredLocale() {
        return preferredLocale;
    }

    @JsonProperty("preferredLocale")
    public void setPreferredLocale(PreferredLocale__ preferredLocale) {
        this.preferredLocale = preferredLocale;
    }

    public Title withPreferredLocale(PreferredLocale__ preferredLocale) {
        this.preferredLocale = preferredLocale;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Title withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("localized", localized).append("preferredLocale", preferredLocale).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(localized).append(additionalProperties).append(preferredLocale).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Title) == false) {
            return false;
        }
        Title rhs = ((Title) other);
        return new EqualsBuilder().append(localized, rhs.localized).append(additionalProperties, rhs.additionalProperties).append(preferredLocale, rhs.preferredLocale).isEquals();
    }

}
