package com.example.riskCalculationModule.linked.service;

import com.example.riskCalculationModule.LinkedInJobEntity;
import com.example.riskCalculationModule.linked.LinkedDao;
import com.example.riskCalculationModule.linked.data.LinkedEntity;
import com.example.riskCalculationModule.linked.data.linkedModel.LinkedEmail;
import com.example.riskCalculationModule.linked.data.linkedModel.LinkedNameAndSurname;
import com.example.riskCalculationModule.linked.linkedinJobs.LinkedJobs;
import com.example.riskCalculationModule.linked.linkedinJobs.StartMonthYear;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@Service
public class LinkedService
{
    private final LinkedDao dao;

    public LinkedService(LinkedDao dao) {
        this.dao = dao;
    }

    public void saveLinkedInfo(String token, String appEmailAddress) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization", "Bearer " + token);
        String url = "https://api.linkedin.com/v2/me?projection=(id,firstName,lastName,profilePicture(displayImage~:playableStreams))";
        String url2 = "https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))";
        HttpEntity<String> entity = new HttpEntity<String>("parameters", httpHeaders);

        ResponseEntity<LinkedNameAndSurname> nameResponse = restTemplate.exchange(url, HttpMethod.GET, entity, LinkedNameAndSurname.class);
        ResponseEntity<LinkedEmail> respone2 = restTemplate.exchange(url2, HttpMethod.GET, entity, LinkedEmail.class);
        LinkedJobs linkedJobs = mockDataJobsFromLinkedIn();


        LinkedInJobEntity linkedInJobEntity = LinkedInJobEntity
                .builder()
                .companyName(linkedJobs.getCompany())
                .startYear(linkedJobs.getStartMonthYear().getYear())
                .startMonth(linkedJobs.getStartMonthYear().getMonth())
                .build();

        LinkedEntity linkedEntity = LinkedEntity
                .builder()
                .name(nameResponse.getBody().getFirstName().getLocalized().getPlPL())
                .surname(nameResponse.getBody().getLastName().getLocalized().getPlPL())
                .email(respone2.getBody().getElements().get(0).getHandl().getEmailAddress())
                .appEmailAddress(appEmailAddress)
                .listOfUsersJobs(Collections.singletonList(linkedInJobEntity))
                .build();

        dao.save(linkedEntity);

    }

    private LinkedJobs mockDataJobsFromLinkedIn()
    {
        LinkedJobs linkedJobs = new LinkedJobs();
        linkedJobs.setCompany("MockedCompanyForLinkedInService");
        linkedJobs.setStartMonthYear(new StartMonthYear().withYear(1992).withMonth(2));
        return linkedJobs;
    }

}