
package com.example.riskCalculationModule.twitter;

import java.util.List;
import javax.validation.Valid;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Description {

    @SerializedName("urls")
    @Expose
    @Valid
    private List<Object> urls = null;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Description() {
    }

    /**
     * 
     * @param urls
     */
    public Description(List<Object> urls) {
        super();
        this.urls = urls;
    }

    public List<Object> getUrls() {
        return urls;
    }

    public void setUrls(List<Object> urls) {
        this.urls = urls;
    }

    public Description withUrls(List<Object> urls) {
        this.urls = urls;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("urls", urls).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(urls).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Description) == false) {
            return false;
        }
        Description rhs = ((Description) other);
        return new EqualsBuilder().append(urls, rhs.urls).isEquals();
    }

}
