
package com.example.riskCalculationModule.twitter;

import javax.validation.Valid;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class TwitterData {

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("id_str")
    @Expose
    private String idStr;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("screen_name")
    @Expose
    private String screenName;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("url")
    @Expose
    private Object url;
    @SerializedName("entities")
    @Expose
    @Valid
    private Entities entities;
    @SerializedName("protected")
    @Expose
    private Boolean _protected;
    @SerializedName("followers_count")
    @Expose
    private Long followersCount;
    @SerializedName("friends_count")
    @Expose
    private Long friendsCount;
    @SerializedName("listed_count")
    @Expose
    private Long listedCount;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("favourites_count")
    @Expose
    private Long favouritesCount;
    @SerializedName("utc_offset")
    @Expose
    private Object utcOffset;
    @SerializedName("time_zone")
    @Expose
    private Object timeZone;
    @SerializedName("geo_enabled")
    @Expose
    private Boolean geoEnabled;
    @SerializedName("verified")
    @Expose
    private Boolean verified;
    @SerializedName("statuses_count")
    @Expose
    private Long statusesCount;
    @SerializedName("lang")
    @Expose
    private Object lang;
    @SerializedName("contributors_enabled")
    @Expose
    private Boolean contributorsEnabled;
    @SerializedName("is_translator")
    @Expose
    private Boolean isTranslator;
    @SerializedName("is_translation_enabled")
    @Expose
    private Boolean isTranslationEnabled;
    @SerializedName("profile_background_color")
    @Expose
    private String profileBackgroundColor;
    @SerializedName("profile_background_image_url")
    @Expose
    private Object profileBackgroundImageUrl;
    @SerializedName("profile_background_image_url_https")
    @Expose
    private Object profileBackgroundImageUrlHttps;
    @SerializedName("profile_background_tile")
    @Expose
    private Boolean profileBackgroundTile;
    @SerializedName("profile_image_url")
    @Expose
    private String profileImageUrl;
    @SerializedName("profile_image_url_https")
    @Expose
    private String profileImageUrlHttps;
    @SerializedName("profile_link_color")
    @Expose
    private String profileLinkColor;
    @SerializedName("profile_sidebar_border_color")
    @Expose
    private String profileSidebarBorderColor;
    @SerializedName("profile_sidebar_fill_color")
    @Expose
    private String profileSidebarFillColor;
    @SerializedName("profile_text_color")
    @Expose
    private String profileTextColor;
    @SerializedName("profile_use_background_image")
    @Expose
    private Boolean profileUseBackgroundImage;
    @SerializedName("has_extended_profile")
    @Expose
    private Boolean hasExtendedProfile;
    @SerializedName("default_profile")
    @Expose
    private Boolean defaultProfile;
    @SerializedName("default_profile_image")
    @Expose
    private Boolean defaultProfileImage;
    @SerializedName("can_media_tag")
    @Expose
    private Boolean canMediaTag;
    @SerializedName("followed_by")
    @Expose
    private Boolean followedBy;
    @SerializedName("following")
    @Expose
    private Boolean following;
    @SerializedName("follow_request_sent")
    @Expose
    private Boolean followRequestSent;
    @SerializedName("notifications")
    @Expose
    private Boolean notifications;
    @SerializedName("translator_type")
    @Expose
    private String translatorType;
    @SerializedName("suspended")
    @Expose
    private Boolean suspended;
    @SerializedName("needs_phone_verification")
    @Expose
    private Boolean needsPhoneVerification;

    /**
     * No args constructor for use in serialization
     * 
     */
    public TwitterData() {
    }

    /**
     * 
     * @param needsPhoneVerification
     * @param geoEnabled
     * @param description
     * @param profileTextColor
     * @param screenName
     * @param contributorsEnabled
     * @param profileUseBackgroundImage
     * @param canMediaTag
     * @param createdAt
     * @param profileBackgroundImageUrl
     * @param _protected
     * @param id
     * @param lang
     * @param profileImageUrl
     * @param hasExtendedProfile
     * @param defaultProfileImage
     * @param idStr
     * @param profileSidebarBorderColor
     * @param statusesCount
     * @param utcOffset
     * @param profileBackgroundTile
     * @param profileBackgroundImageUrlHttps
     * @param verified
     * @param profileBackgroundColor
     * @param favouritesCount
     * @param timeZone
     * @param friendsCount
     * @param defaultProfile
     * @param profileLinkColor
     * @param isTranslator
     * @param listedCount
     * @param url
     * @param suspended
     * @param profileSidebarFillColor
     * @param isTranslationEnabled
     * @param followedBy
     * @param translatorType
     * @param entities
     * @param profileImageUrlHttps
     * @param following
     * @param name
     * @param location
     * @param followersCount
     * @param notifications
     * @param followRequestSent
     */
    public TwitterData(Long id, String idStr, String name, String screenName, String location, String description, Object url, Entities entities, Boolean _protected, Long followersCount, Long friendsCount, Long listedCount, String createdAt, Long favouritesCount, Object utcOffset, Object timeZone, Boolean geoEnabled, Boolean verified, Long statusesCount, Object lang, Boolean contributorsEnabled, Boolean isTranslator, Boolean isTranslationEnabled, String profileBackgroundColor, Object profileBackgroundImageUrl, Object profileBackgroundImageUrlHttps, Boolean profileBackgroundTile, String profileImageUrl, String profileImageUrlHttps, String profileLinkColor, String profileSidebarBorderColor, String profileSidebarFillColor, String profileTextColor, Boolean profileUseBackgroundImage, Boolean hasExtendedProfile, Boolean defaultProfile, Boolean defaultProfileImage, Boolean canMediaTag, Boolean followedBy, Boolean following, Boolean followRequestSent, Boolean notifications, String translatorType, Boolean suspended, Boolean needsPhoneVerification) {
        super();
        this.id = id;
        this.idStr = idStr;
        this.name = name;
        this.screenName = screenName;
        this.location = location;
        this.description = description;
        this.url = url;
        this.entities = entities;
        this._protected = _protected;
        this.followersCount = followersCount;
        this.friendsCount = friendsCount;
        this.listedCount = listedCount;
        this.createdAt = createdAt;
        this.favouritesCount = favouritesCount;
        this.utcOffset = utcOffset;
        this.timeZone = timeZone;
        this.geoEnabled = geoEnabled;
        this.verified = verified;
        this.statusesCount = statusesCount;
        this.lang = lang;
        this.contributorsEnabled = contributorsEnabled;
        this.isTranslator = isTranslator;
        this.isTranslationEnabled = isTranslationEnabled;
        this.profileBackgroundColor = profileBackgroundColor;
        this.profileBackgroundImageUrl = profileBackgroundImageUrl;
        this.profileBackgroundImageUrlHttps = profileBackgroundImageUrlHttps;
        this.profileBackgroundTile = profileBackgroundTile;
        this.profileImageUrl = profileImageUrl;
        this.profileImageUrlHttps = profileImageUrlHttps;
        this.profileLinkColor = profileLinkColor;
        this.profileSidebarBorderColor = profileSidebarBorderColor;
        this.profileSidebarFillColor = profileSidebarFillColor;
        this.profileTextColor = profileTextColor;
        this.profileUseBackgroundImage = profileUseBackgroundImage;
        this.hasExtendedProfile = hasExtendedProfile;
        this.defaultProfile = defaultProfile;
        this.defaultProfileImage = defaultProfileImage;
        this.canMediaTag = canMediaTag;
        this.followedBy = followedBy;
        this.following = following;
        this.followRequestSent = followRequestSent;
        this.notifications = notifications;
        this.translatorType = translatorType;
        this.suspended = suspended;
        this.needsPhoneVerification = needsPhoneVerification;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TwitterData withId(Long id) {
        this.id = id;
        return this;
    }

    public String getIdStr() {
        return idStr;
    }

    public void setIdStr(String idStr) {
        this.idStr = idStr;
    }

    public TwitterData withIdStr(String idStr) {
        this.idStr = idStr;
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TwitterData withName(String name) {
        this.name = name;
        return this;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public TwitterData withScreenName(String screenName) {
        this.screenName = screenName;
        return this;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public TwitterData withLocation(String location) {
        this.location = location;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TwitterData withDescription(String description) {
        this.description = description;
        return this;
    }

    public Object getUrl() {
        return url;
    }

    public void setUrl(Object url) {
        this.url = url;
    }

    public TwitterData withUrl(Object url) {
        this.url = url;
        return this;
    }

    public Entities getEntities() {
        return entities;
    }

    public void setEntities(Entities entities) {
        this.entities = entities;
    }

    public TwitterData withEntities(Entities entities) {
        this.entities = entities;
        return this;
    }

    public Boolean getProtected() {
        return _protected;
    }

    public void setProtected(Boolean _protected) {
        this._protected = _protected;
    }

    public TwitterData withProtected(Boolean _protected) {
        this._protected = _protected;
        return this;
    }

    public Long getFollowersCount() {
        return followersCount;
    }

    public void setFollowersCount(Long followersCount) {
        this.followersCount = followersCount;
    }

    public TwitterData withFollowersCount(Long followersCount) {
        this.followersCount = followersCount;
        return this;
    }

    public Long getFriendsCount() {
        return friendsCount;
    }

    public void setFriendsCount(Long friendsCount) {
        this.friendsCount = friendsCount;
    }

    public TwitterData withFriendsCount(Long friendsCount) {
        this.friendsCount = friendsCount;
        return this;
    }

    public Long getListedCount() {
        return listedCount;
    }

    public void setListedCount(Long listedCount) {
        this.listedCount = listedCount;
    }

    public TwitterData withListedCount(Long listedCount) {
        this.listedCount = listedCount;
        return this;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public TwitterData withCreatedAt(String createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public Long getFavouritesCount() {
        return favouritesCount;
    }

    public void setFavouritesCount(Long favouritesCount) {
        this.favouritesCount = favouritesCount;
    }

    public TwitterData withFavouritesCount(Long favouritesCount) {
        this.favouritesCount = favouritesCount;
        return this;
    }

    public Object getUtcOffset() {
        return utcOffset;
    }

    public void setUtcOffset(Object utcOffset) {
        this.utcOffset = utcOffset;
    }

    public TwitterData withUtcOffset(Object utcOffset) {
        this.utcOffset = utcOffset;
        return this;
    }

    public Object getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(Object timeZone) {
        this.timeZone = timeZone;
    }

    public TwitterData withTimeZone(Object timeZone) {
        this.timeZone = timeZone;
        return this;
    }

    public Boolean getGeoEnabled() {
        return geoEnabled;
    }

    public void setGeoEnabled(Boolean geoEnabled) {
        this.geoEnabled = geoEnabled;
    }

    public TwitterData withGeoEnabled(Boolean geoEnabled) {
        this.geoEnabled = geoEnabled;
        return this;
    }

    public Boolean getVerified() {
        return verified;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }

    public TwitterData withVerified(Boolean verified) {
        this.verified = verified;
        return this;
    }

    public Long getStatusesCount() {
        return statusesCount;
    }

    public void setStatusesCount(Long statusesCount) {
        this.statusesCount = statusesCount;
    }

    public TwitterData withStatusesCount(Long statusesCount) {
        this.statusesCount = statusesCount;
        return this;
    }

    public Object getLang() {
        return lang;
    }

    public void setLang(Object lang) {
        this.lang = lang;
    }

    public TwitterData withLang(Object lang) {
        this.lang = lang;
        return this;
    }

    public Boolean getContributorsEnabled() {
        return contributorsEnabled;
    }

    public void setContributorsEnabled(Boolean contributorsEnabled) {
        this.contributorsEnabled = contributorsEnabled;
    }

    public TwitterData withContributorsEnabled(Boolean contributorsEnabled) {
        this.contributorsEnabled = contributorsEnabled;
        return this;
    }

    public Boolean getIsTranslator() {
        return isTranslator;
    }

    public void setIsTranslator(Boolean isTranslator) {
        this.isTranslator = isTranslator;
    }

    public TwitterData withIsTranslator(Boolean isTranslator) {
        this.isTranslator = isTranslator;
        return this;
    }

    public Boolean getIsTranslationEnabled() {
        return isTranslationEnabled;
    }

    public void setIsTranslationEnabled(Boolean isTranslationEnabled) {
        this.isTranslationEnabled = isTranslationEnabled;
    }

    public TwitterData withIsTranslationEnabled(Boolean isTranslationEnabled) {
        this.isTranslationEnabled = isTranslationEnabled;
        return this;
    }

    public String getProfileBackgroundColor() {
        return profileBackgroundColor;
    }

    public void setProfileBackgroundColor(String profileBackgroundColor) {
        this.profileBackgroundColor = profileBackgroundColor;
    }

    public TwitterData withProfileBackgroundColor(String profileBackgroundColor) {
        this.profileBackgroundColor = profileBackgroundColor;
        return this;
    }

    public Object getProfileBackgroundImageUrl() {
        return profileBackgroundImageUrl;
    }

    public void setProfileBackgroundImageUrl(Object profileBackgroundImageUrl) {
        this.profileBackgroundImageUrl = profileBackgroundImageUrl;
    }

    public TwitterData withProfileBackgroundImageUrl(Object profileBackgroundImageUrl) {
        this.profileBackgroundImageUrl = profileBackgroundImageUrl;
        return this;
    }

    public Object getProfileBackgroundImageUrlHttps() {
        return profileBackgroundImageUrlHttps;
    }

    public void setProfileBackgroundImageUrlHttps(Object profileBackgroundImageUrlHttps) {
        this.profileBackgroundImageUrlHttps = profileBackgroundImageUrlHttps;
    }

    public TwitterData withProfileBackgroundImageUrlHttps(Object profileBackgroundImageUrlHttps) {
        this.profileBackgroundImageUrlHttps = profileBackgroundImageUrlHttps;
        return this;
    }

    public Boolean getProfileBackgroundTile() {
        return profileBackgroundTile;
    }

    public void setProfileBackgroundTile(Boolean profileBackgroundTile) {
        this.profileBackgroundTile = profileBackgroundTile;
    }

    public TwitterData withProfileBackgroundTile(Boolean profileBackgroundTile) {
        this.profileBackgroundTile = profileBackgroundTile;
        return this;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public TwitterData withProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
        return this;
    }

    public String getProfileImageUrlHttps() {
        return profileImageUrlHttps;
    }

    public void setProfileImageUrlHttps(String profileImageUrlHttps) {
        this.profileImageUrlHttps = profileImageUrlHttps;
    }

    public TwitterData withProfileImageUrlHttps(String profileImageUrlHttps) {
        this.profileImageUrlHttps = profileImageUrlHttps;
        return this;
    }

    public String getProfileLinkColor() {
        return profileLinkColor;
    }

    public void setProfileLinkColor(String profileLinkColor) {
        this.profileLinkColor = profileLinkColor;
    }

    public TwitterData withProfileLinkColor(String profileLinkColor) {
        this.profileLinkColor = profileLinkColor;
        return this;
    }

    public String getProfileSidebarBorderColor() {
        return profileSidebarBorderColor;
    }

    public void setProfileSidebarBorderColor(String profileSidebarBorderColor) {
        this.profileSidebarBorderColor = profileSidebarBorderColor;
    }

    public TwitterData withProfileSidebarBorderColor(String profileSidebarBorderColor) {
        this.profileSidebarBorderColor = profileSidebarBorderColor;
        return this;
    }

    public String getProfileSidebarFillColor() {
        return profileSidebarFillColor;
    }

    public void setProfileSidebarFillColor(String profileSidebarFillColor) {
        this.profileSidebarFillColor = profileSidebarFillColor;
    }

    public TwitterData withProfileSidebarFillColor(String profileSidebarFillColor) {
        this.profileSidebarFillColor = profileSidebarFillColor;
        return this;
    }

    public String getProfileTextColor() {
        return profileTextColor;
    }

    public void setProfileTextColor(String profileTextColor) {
        this.profileTextColor = profileTextColor;
    }

    public TwitterData withProfileTextColor(String profileTextColor) {
        this.profileTextColor = profileTextColor;
        return this;
    }

    public Boolean getProfileUseBackgroundImage() {
        return profileUseBackgroundImage;
    }

    public void setProfileUseBackgroundImage(Boolean profileUseBackgroundImage) {
        this.profileUseBackgroundImage = profileUseBackgroundImage;
    }

    public TwitterData withProfileUseBackgroundImage(Boolean profileUseBackgroundImage) {
        this.profileUseBackgroundImage = profileUseBackgroundImage;
        return this;
    }

    public Boolean getHasExtendedProfile() {
        return hasExtendedProfile;
    }

    public void setHasExtendedProfile(Boolean hasExtendedProfile) {
        this.hasExtendedProfile = hasExtendedProfile;
    }

    public TwitterData withHasExtendedProfile(Boolean hasExtendedProfile) {
        this.hasExtendedProfile = hasExtendedProfile;
        return this;
    }

    public Boolean getDefaultProfile() {
        return defaultProfile;
    }

    public void setDefaultProfile(Boolean defaultProfile) {
        this.defaultProfile = defaultProfile;
    }

    public TwitterData withDefaultProfile(Boolean defaultProfile) {
        this.defaultProfile = defaultProfile;
        return this;
    }

    public Boolean getDefaultProfileImage() {
        return defaultProfileImage;
    }

    public void setDefaultProfileImage(Boolean defaultProfileImage) {
        this.defaultProfileImage = defaultProfileImage;
    }

    public TwitterData withDefaultProfileImage(Boolean defaultProfileImage) {
        this.defaultProfileImage = defaultProfileImage;
        return this;
    }

    public Boolean getCanMediaTag() {
        return canMediaTag;
    }

    public void setCanMediaTag(Boolean canMediaTag) {
        this.canMediaTag = canMediaTag;
    }

    public TwitterData withCanMediaTag(Boolean canMediaTag) {
        this.canMediaTag = canMediaTag;
        return this;
    }

    public Boolean getFollowedBy() {
        return followedBy;
    }

    public void setFollowedBy(Boolean followedBy) {
        this.followedBy = followedBy;
    }

    public TwitterData withFollowedBy(Boolean followedBy) {
        this.followedBy = followedBy;
        return this;
    }

    public Boolean getFollowing() {
        return following;
    }

    public void setFollowing(Boolean following) {
        this.following = following;
    }

    public TwitterData withFollowing(Boolean following) {
        this.following = following;
        return this;
    }

    public Boolean getFollowRequestSent() {
        return followRequestSent;
    }

    public void setFollowRequestSent(Boolean followRequestSent) {
        this.followRequestSent = followRequestSent;
    }

    public TwitterData withFollowRequestSent(Boolean followRequestSent) {
        this.followRequestSent = followRequestSent;
        return this;
    }

    public Boolean getNotifications() {
        return notifications;
    }

    public void setNotifications(Boolean notifications) {
        this.notifications = notifications;
    }

    public TwitterData withNotifications(Boolean notifications) {
        this.notifications = notifications;
        return this;
    }

    public String getTranslatorType() {
        return translatorType;
    }

    public void setTranslatorType(String translatorType) {
        this.translatorType = translatorType;
    }

    public TwitterData withTranslatorType(String translatorType) {
        this.translatorType = translatorType;
        return this;
    }

    public Boolean getSuspended() {
        return suspended;
    }

    public void setSuspended(Boolean suspended) {
        this.suspended = suspended;
    }

    public TwitterData withSuspended(Boolean suspended) {
        this.suspended = suspended;
        return this;
    }

    public Boolean getNeedsPhoneVerification() {
        return needsPhoneVerification;
    }

    public void setNeedsPhoneVerification(Boolean needsPhoneVerification) {
        this.needsPhoneVerification = needsPhoneVerification;
    }

    public TwitterData withNeedsPhoneVerification(Boolean needsPhoneVerification) {
        this.needsPhoneVerification = needsPhoneVerification;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("idStr", idStr).append("name", name).append("screenName", screenName).append("location", location).append("description", description).append("url", url).append("entities", entities).append("_protected", _protected).append("followersCount", followersCount).append("friendsCount", friendsCount).append("listedCount", listedCount).append("createdAt", createdAt).append("favouritesCount", favouritesCount).append("utcOffset", utcOffset).append("timeZone", timeZone).append("geoEnabled", geoEnabled).append("verified", verified).append("statusesCount", statusesCount).append("lang", lang).append("contributorsEnabled", contributorsEnabled).append("isTranslator", isTranslator).append("isTranslationEnabled", isTranslationEnabled).append("profileBackgroundColor", profileBackgroundColor).append("profileBackgroundImageUrl", profileBackgroundImageUrl).append("profileBackgroundImageUrlHttps", profileBackgroundImageUrlHttps).append("profileBackgroundTile", profileBackgroundTile).append("profileImageUrl", profileImageUrl).append("profileImageUrlHttps", profileImageUrlHttps).append("profileLinkColor", profileLinkColor).append("profileSidebarBorderColor", profileSidebarBorderColor).append("profileSidebarFillColor", profileSidebarFillColor).append("profileTextColor", profileTextColor).append("profileUseBackgroundImage", profileUseBackgroundImage).append("hasExtendedProfile", hasExtendedProfile).append("defaultProfile", defaultProfile).append("defaultProfileImage", defaultProfileImage).append("canMediaTag", canMediaTag).append("followedBy", followedBy).append("following", following).append("followRequestSent", followRequestSent).append("notifications", notifications).append("translatorType", translatorType).append("suspended", suspended).append("needsPhoneVerification", needsPhoneVerification).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(needsPhoneVerification).append(geoEnabled).append(description).append(profileTextColor).append(screenName).append(contributorsEnabled).append(profileUseBackgroundImage).append(canMediaTag).append(createdAt).append(profileBackgroundImageUrl).append(_protected).append(id).append(lang).append(profileImageUrl).append(hasExtendedProfile).append(defaultProfileImage).append(idStr).append(profileSidebarBorderColor).append(statusesCount).append(utcOffset).append(profileBackgroundTile).append(profileBackgroundImageUrlHttps).append(verified).append(profileBackgroundColor).append(favouritesCount).append(timeZone).append(friendsCount).append(defaultProfile).append(profileLinkColor).append(isTranslator).append(listedCount).append(url).append(suspended).append(profileSidebarFillColor).append(isTranslationEnabled).append(followedBy).append(translatorType).append(entities).append(profileImageUrlHttps).append(following).append(name).append(location).append(followersCount).append(notifications).append(followRequestSent).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof TwitterData) == false) {
            return false;
        }
        TwitterData rhs = ((TwitterData) other);
        return new EqualsBuilder().append(needsPhoneVerification, rhs.needsPhoneVerification).append(geoEnabled, rhs.geoEnabled).append(description, rhs.description).append(profileTextColor, rhs.profileTextColor).append(screenName, rhs.screenName).append(contributorsEnabled, rhs.contributorsEnabled).append(profileUseBackgroundImage, rhs.profileUseBackgroundImage).append(canMediaTag, rhs.canMediaTag).append(createdAt, rhs.createdAt).append(profileBackgroundImageUrl, rhs.profileBackgroundImageUrl).append(_protected, rhs._protected).append(id, rhs.id).append(lang, rhs.lang).append(profileImageUrl, rhs.profileImageUrl).append(hasExtendedProfile, rhs.hasExtendedProfile).append(defaultProfileImage, rhs.defaultProfileImage).append(idStr, rhs.idStr).append(profileSidebarBorderColor, rhs.profileSidebarBorderColor).append(statusesCount, rhs.statusesCount).append(utcOffset, rhs.utcOffset).append(profileBackgroundTile, rhs.profileBackgroundTile).append(profileBackgroundImageUrlHttps, rhs.profileBackgroundImageUrlHttps).append(verified, rhs.verified).append(profileBackgroundColor, rhs.profileBackgroundColor).append(favouritesCount, rhs.favouritesCount).append(timeZone, rhs.timeZone).append(friendsCount, rhs.friendsCount).append(defaultProfile, rhs.defaultProfile).append(profileLinkColor, rhs.profileLinkColor).append(isTranslator, rhs.isTranslator).append(listedCount, rhs.listedCount).append(url, rhs.url).append(suspended, rhs.suspended).append(profileSidebarFillColor, rhs.profileSidebarFillColor).append(isTranslationEnabled, rhs.isTranslationEnabled).append(followedBy, rhs.followedBy).append(translatorType, rhs.translatorType).append(entities, rhs.entities).append(profileImageUrlHttps, rhs.profileImageUrlHttps).append(following, rhs.following).append(name, rhs.name).append(location, rhs.location).append(followersCount, rhs.followersCount).append(notifications, rhs.notifications).append(followRequestSent, rhs.followRequestSent).isEquals();
    }

}
